# Author: Ruslan Krenzler
# July 2021
# Approximate system with fixed utilization.


# Load approximation functions.
source("optimal-robots.R")
# Load teset systems.
source("test-system.R")
N <- 50 # Set maximal number of robots
# Store data to this directory.
data_dir <- "../data"

initial_lambda.bo <-468/3600

# This is not an effective code
model_utilization<-function(lambda.bo, n){
  # Approximation utilization of a model with
  # Arrival rate lambda.bo and n number of robots.
  
  newlambdaLC <- AdjustArrivalLC(lambda.bo = lambda.bo,
                               nodes,
                               r = r,
                               n = n)
  # Calculate internal waiting times.
  model <- EffectiveArrivals(newlambdaLC, nodes, r, n)$model
  L.pool <-PoolLength(InnerQueueLengths(model))
  utilization <-  1-L.pool/n
  return(utilization)
}

find_lambda_bo<-function(n, utilization, lower_lambda.bo, upper_lambda.bo){
  # Find arrival rate lambda.bo for a model with "n" robots,
  # such that the utlization is equal "utilization"
  f<-function(lambda.bo){model_utilization(lambda.bo, n)-utilization}
  res<-uniroot(f, lower=lower_lambda.bo, upper=upper_lambda.bo)
  #res<-uniroot(f, lower=lower_lambda.bo, upper=upper_lambda.bo, tol=0.00001, maxiter = 10000)  # Does not changes much
  return(res$root)
}


# arrival rates for 1-N robots such that utilization is 95% and 80%.
max_lambdas <- MaxArrivalRates(nodes, r, max.n = N)
lambda_bos<-data.frame()
appx_utilizations <- c(0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 0.95)
for (n in 1:N){
  for (ut in appx_utilizations){
  # start a little below the maximal arrival
    lambda.bo <- find_lambda_bo(n, ut, 0.001, max_lambdas[n]*0.9999)
    lambda_bos <- rbind(lambda_bos, list(Robots=n, ArrivalRate=lambda.bo, AppxUtilization=ut))
  }
}
# We better save it as "Robots", "ArrivalRate", "AppxUtilization"
path <- paste(data_dir, "appx-arrival-rates-for-utilization.csv", sep = "/")
write.csv(lambda_bos, path, row.names = FALSE)

appx_results <- lambda_bos
# Add empty rows
appx_results$AdjustedArrivalRate <- NA
appx_results$TurnOver <- NA
appx_results$External <- NA
appx_results$WaitingForPicker <- NA
appx_results$WaitingForReplenisher <- NA
appx_results$RobotIdleTime <- NA
appx_results$PoolLength <- NA
appx_results$UtilizationControl <- NA  # Remove this later.
appx_results$Empty0Prob <- NA  # Probability for 0 to be empty.


# Create empty throuputs and inner waiting times
thDf <- data.frame()
innerWaitingDf <- data.frame()
idleProbDf <- data.frame()

for (i in 1:nrow(appx_results)){
  row = appx_results[i,]
  n = row$Robots[[1]]
  
  #print(lambda_bos_row)
  # Ajust lambda
  lambda.bo = row$ArrivalRate[[1]]
  
  
  newlambdaLC <- AdjustArrivalLC(lambda.bo = lambda.bo,
                                 nodes,
                                 r = r,
                                 n = n)
  appx_results$AdjustedArrivalRate[i] <- newlambdaLC
  # Calculate internal waiting times.
  model <- EffectiveArrivals(newlambdaLC, nodes, r, n)$model
  
  # Store all throuputs.
  th <- as.data.frame(Throughputk(model))
  # Add utilization and robot information to the trouput data
  th$AppxUtilization<-row$AppxUtilization[[1]]
  th$Robots<-row$Robots[[1]]
  thDf <- rbind(thDf, th)
  
  # Store idle probabilities
  
  # Store all inner waiting times.
  w <- Wk(model)
  # Restore names
  #names(w) <- names(model$Inputs$nodes)
  w <- as.data.frame(w)
  # Add utilization and robot information.
  w$AppxUtilization<-row$AppxUtilization[[1]]
  w$Robots<-row$Robots[[1]]
  innerWaitingDf <- rbind(innerWaitingDf, w)
  
  appx_results$TurnOver[i] <- SpecialInnerWaitingTime(InnerWaitingTimes(model), rates)
  appx_results$External[i] <- ExternalAppx(lambda.bo = lambda.bo, nodes, r, n)$W.ex
  appx_results$WaitingForPicker[i] <- WaitingForPickerTime(InnerWaitingTimes(model), rates)
  appx_results$WaitingForReplenisher[i] <- WaitingForReplenisherTime(InnerWaitingTimes(model), rates)
  appx_results$RobotIdleTime[i] <- IdleTime(InnerWaitingTimes(model))
  appx_results$PoolLength[i] <- PoolLength(InnerQueueLengths(model))
  appx_results$UtilizationControl[i] <- 1-appx_results$PoolLength[i]/appx_results$Robots[i]  # Remove this later
  appx_results$Empty0Prob[i] <- 1-lambda.bo/newlambdaLC
}

appx_results$Total <- appx_results$TurnOver + appx_results$External
appx_results$AppxUtilizationControl<-1-appx_results$PoolLength/appx_results$Robots

# Plot waiting times.
waitingDf <- subset(appx_results, AppxUtilization==0.80,  select = c(Robots,TurnOver,External,RobotIdleTime))
rownames(waitingDf) <- waitingDf$Robots
waitingDf$Robots<-NULL
barplot(t(as.matrix(waitingDf)), col = c("blue", "red", "green"), main="Utilization=80%")
legend("topright", c("turn over", "external", "robots' idle time"), fill = c("blue", "red", "green"))

waitingDf <- subset(appx_results, AppxUtilization==0.95,  select = c(Robots,TurnOver,External,RobotIdleTime))
rownames(waitingDf) <- waitingDf$Robots
waitingDf$Robots<-NULL
barplot(t(as.matrix(waitingDf)), col = c("blue", "red", "green"), main="Utilization=95%")
legend("topright", c("turn over", "external", "robots' idle time"), fill = c("blue", "red", "green"))

# Store waiting times for all robots.
path <- paste(data_dir, "appx-for-utilization.csv", sep = "/")
write.csv(appx_results, path, row.names = FALSE)
for(ut in appx_utilizations){
  filename=sprintf("appx-for-utilization-u%d.csv", round(ut*100))
  path <- paste(data_dir, filename, sep = "/")
  write.csv(subset(appx_results,  AppxUtilization==ut) , path, row.names = FALSE)
  
  # Save inner waiting times.
  filename=sprintf("appx-inner-wating-times-u%d.csv", round(ut*100))
  path <- paste(data_dir, filename, sep = "/")
  write.csv(subset(innerWaitingDf,  AppxUtilization==ut), path, row.names = FALSE)
}

# Save throuputs.
path <- paste(data_dir, "all-nummeric-throughputs-for-utilization.csv", sep = "/")
write.csv(thDf, path, row.names = FALSE)


# Save idle times.
#path <- paste(data_dir, "appx-idle-time-prob-u95.csv", sep = "/")
#idle <- IdleTimeProb(lambda.bo=lambda.bo, nodes=nodes, r=r)
# Save it to the data frame
#idleDf<-data.frame(Node=names(idle), Prob=unlist(idle))
#write.csv(idleDf, path, row.names = FALSE)

