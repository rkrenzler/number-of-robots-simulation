# Queueing model of a robotic mobile fulfillment system.
# Copyright (C) 2019 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Simulate a system with b

.. moduleauthor: Ruslan Krenzler, Sonja Otten

11. September 2019
"""

import random
import array
import simpy
import pandas
import itertools

# ----------------------- Initialisation ------------------------------------------------------------------------------
# Initialize a pseudorandom number generator to get reproducible results
# We will create other random seeds from it and use these seeds for
# parallel simulations


# Different simulation times
SIM_TIME = 3600  # An hour
# SIM_TIME = 24*3600 # A day.
# SIM_TIME = 2*24*3600 # Two days.
# SIM_TIME = 10*24*3600 # 10 Days.
# SIM_TIME = 365*24*3600 # A year.
NUMBER_OF_ROBOTS: int = 17  # total number of robots
NUMBER_OF_PICKER: int = 1  # total number of pickers at the pick station (1 is equal to Single Server)

# Rates in seconds.
ARRIVAL_RATE = 0.13  # arrival rate of orders
MOVING_TO_POD_RATE = 1.0 / 18.4  # travelling rate of the robot to the pod in the storage area
MOVING_TO_PICKER_RATES = {1: 1 / 34.5, 2: 1 / 34.5}  # travelling rate of the robot with the pod to the pick station
PICKING_RATES = {1: 1 / 10, 2: 1 / 10}  # picking rate of the picker (without drift space)
NUMBERS_OF_PICKERS = {1: 1, 2: 1}  # Number of pickers in each station.
PICKER_PROB = {1: 0.5, 2: 0.5}  # Probability to select a particular picker
MOVING_TO_STORAGE_RATES = {1: 1 / 34.5, 2: 1 / 34.5}  # travelling rate of the robot to carry back the pod
REPLANISHMENT_RATE = 1 / 30
REPLENISHMENT_PROB = 0.2
MOVING_TO_REPLENISHMENT_RATES = {1: 1 / 34.5, 2: 1 / 34.5}
MOVING_FROM_REPLENISHMENT_RATE = 1 / 34.5
NUMBER_OF_REPLENISHER: int = 1  # Number of replenisher at the replenishment station.


def to_replenishmet():
    """Randomly decide to go to replenishment

    @:return
        * True with probability TO_REPLENISHMENT_PROB,
        * False otherwise
    """
    return random.choices([True, False], weights=(REPLENISHMENT_PROB, 1 - REPLENISHMENT_PROB))[0]


def select_picker(pickers):
    """Randomly select a picker with probability PICKER_PROB.

    @:return Random key from the pickers dictionary
    """
    ks = list(pickers.keys())
    w = [PICKER_PROB[k] for k in ks]
    return random.choices(ks, weights=w)[0]


def interrarival_time(type, arrival_rate):
    """Generate a random expontentail or deterministic interrarival time."""

    if type == "exp":
        return random.expovariate(arrival_rate)
    elif type == "det":
        return 1 / arrival_rate
    else:
        raise Exception("Unknown interarrival_type {}.".fromat(type))


def source(env, robots, pickers, replenishment, waiting_times, type, arrival_rate, total_t):
    """Source generates orders randomly (exponentially distributed inter-arrival times).
    Each order obtains exponentially distributed times
    for the moving to the pod, to the picker, and to the storage area"""
    last_time_printed = env.now
    for i in itertools.count():
        # Calculate random numbers in advance: they include service times
        # and random picker.
        srv_times = {}
        # Exponentially distributed travelling time of the robot to the pod.
        srv_times["to_pod"] = random.expovariate(MOVING_TO_POD_RATE)
        picker_id = select_picker(pickers)
        # Exponentially distributed travelling time of the robot with the pod to the pick station.
        srv_times["to_picker"] = random.expovariate(MOVING_TO_PICKER_RATES[picker_id])
        # Exponentially distributed picking time
        srv_times["picking"] = random.expovariate(PICKING_RATES[picker_id])
        # Exponentially distributed travelling time of the robot to carry back the pod to the storage area.
        srv_times["to_storage"] = random.expovariate(MOVING_TO_STORAGE_RATES[picker_id])

        # Randomly select to go to replenishment
        if to_replenishmet():
            srv_times["to_replenishment"] = random.expovariate(MOVING_TO_REPLENISHMENT_RATES[picker_id])
            srv_times["replenishment"] = random.expovariate(REPLANISHMENT_RATE)
            srv_times["from_replenishment"] = random.expovariate(MOVING_FROM_REPLENISHMENT_RATE)

        c = order(env, i, robots, pickers, picker_id, replenishment, srv_times, waiting_times)
        env.process(c)

        # Wait for exponentially or deterministic distributed inter-arrival times of orders.
        yield env.timeout(interrarival_time(type, arrival_rate))
        # Show each 10 % percent that simulation was finished.
        SPAN = total_t / 10
        if env.now - last_time_printed > SPAN:
            last_time_printed = env.now
            print("{0}% complete".format(env.now / total_t * 100))


def create_empty_waiting_times():
    return {
        "OrderId": array.array('i'),
        "Arrival": array.array('f'),
        "PickerId": array.array('i'),
        "External": array.array('f'),
        "ToPod": array.array('f'),
        "ToPicker": array.array('f'),
        "WaitingForPicker": array.array('f'),
        "TurnOver": array.array('f'),
        "Picker": array.array('f'),
        "ToStorage": array.array('f'),
        "ToReplenishment": array.array('f'),
        "WaitingForReplenisher": array.array('f'),
        "Replenishment": array.array('f'),
        "FromReplenishment": array.array('f'),
        "RobotBusy": array.array('f'),
    }


def order(env, order_id, robots, pickers, picker_id, replenishment, srv_times, waiting_times):
    """Orders arrive one by one according to a Poisson process.
    Each order requires an idle robot, which is taken from the ‘virtual’ queue of idle robots.
    If there is no idle robot, the order will wait (backordering).

    After the match of the idle robot and the order, the robot moves to the pod.
    This movement is modelled by an infinite server
    with exponentially distributed service times with rate MOVING_TO_PICKER_RATE.

    Then the robot lifts this pod and moves the pod from the storage area to the pick station.
    This movement is modelled by an infinite server
    with exponentially distributed service times and rate MOVING_TO_PICKER_RATE.

    At the pick station robots queue until it is their turn.

    The pick station consists of a single server with finite waiting room
    who picks storage items from the pod under a FCFS regime.
    The picking time follows an exponential distribution with rate PICKING_RATE.

    If the picker has completed picking, the robot carries the pod back to the storage area.
    This movement is modelled by an infinite server
    with exponentially distributed service times and rate MOVING_TO_STORAGE_RATE.

    After this movement the robot enters the 'virtual’ queue of idle robots."""

    # Calculation of arrival time of order.
    # data_row = {"OrderId":order_id}
    waiting_times["OrderId"].append(order_id)
    order_arrive = env.now  # arrival time
    # data_row["Arrival"] = order_arrive
    waiting_times["Arrival"].append(order_arrive);
    # Store the picker information
    # data_row = {"PickerId":picker_id}
    waiting_times["PickerId"].append(picker_id);
    picker = pickers[picker_id]
    # Store arrival time.
    # Order requests for an idle robot (Backordering).
    # Wait for an idle robot
    with robots.request() as req:
        yield req
        resource_start = env.now
        # Save waiting time for an idle robot.
        # data_row["External"] = resource_start - order_arrive
        waiting_times["External"].append(resource_start - order_arrive)

        # ----- After the match of the idle robot and the order, the robot moves to the pod.
        yield env.timeout(srv_times["to_pod"])
        # Store transportation time.
        # data_row["ToPod"] = srv_times["to_pod"]
        waiting_times["ToPod"].append(srv_times["to_pod"])
        # Then move to the picker.
        yield env.timeout(srv_times["to_picker"])
        # Store transportation time.
        # data_row["ToPicker"] = srv_times["to_picker"]
        waiting_times["ToPicker"].append(srv_times["to_picker"])

        # Calculation of arrival time at pick station.
        arrive_to_pickstation = env.now  # time when robot arrives at pick station
        # ----- At the pick station robots queue until it is their turn.
        # Wait for the idle picker.
        with picker.request() as req_picker:
            yield req_picker
            # Picker is now available. Store the times.
            # data_row["WaitingForPicker"] = env.now - arrive_to_pickstation
            waiting_times["WaitingForPicker"].append(env.now - arrive_to_pickstation)
            # data_row["TurnOver"] = env.now - resource_start
            waiting_times["TurnOver"].append(env.now - resource_start)
            # ----- Picking process starts.
            yield env.timeout(srv_times["picking"])
            # Store time time spend at the picking station.
            # data_row["Picker"] = env.now - arrive_to_pickstation
            waiting_times["Picker"].append(env.now - arrive_to_pickstation)

        # Picker is released.
        # Go to replenishment if there is corresponding data.
        if "replenishment" in srv_times.keys():
            yield env.timeout(srv_times["to_replenishment"])
            # data_row["ToReplenishment"] = srv_times["to_replenishment"]
            waiting_times["ToReplenishment"].append(srv_times["to_replenishment"])
            # Wait for an idle employee.
            arrived_to_replenishment = env.now
            with replenishment.request() as req_replenishment:
                yield req_replenishment
                waiting_times["WaitingForReplenisher"].append(env.now - arrived_to_replenishment)
                # Wait at replenishment complete.
                yield env.timeout(srv_times["replenishment"])
                # Replenishment is complete. Store results
                # data_row["Replenishment"] = env.now - arrived_to_replenishment
                waiting_times["Replenishment"].append(env.now - arrived_to_replenishment)

            # Replenishing employee is released. Go back to the storage.
            yield env.timeout(srv_times["from_replenishment"])
            # data_row["FromReplenishment"] = srv_times["from_replenishment"]
            waiting_times["FromReplenishment"].append(srv_times["from_replenishment"])
        else:
            # No replenishment data. Go directly to storage and store zero waiting times.
            # data_row["ToReplenishment"] = 0
            waiting_times["ToReplenishment"].append(0)
            # data_row["Replenishment"] = 0
            waiting_times["Replenishment"].append(0)
            # data_row["FromReplenishment"] = 0
            waiting_times["FromReplenishment"].append(0)
            yield env.timeout(srv_times["to_storage"])
            # data_row["ToStorage"] = srv_times["to_storage"]
            waiting_times["ToStorage"].append(srv_times["to_storage"])
        # Save the total amount of time the robot was used untill 	
        waiting_times["RobotBusy"].append(env.now - resource_start)
    # waiting_times.append(data_row)


def wt_to_data_frame(waiting_times):
    orders = waiting_times["OrderId"]
    # Make all columns with the same length as orders.
    for (cname, cvals) in waiting_times.items():
        to_add = len(orders) - len(cvals)
        while to_add > 0:
            if (cvals.typecode == "i"):
                cvals.append(0)
            if (cvals.typecode == "f"):
                cvals.append(float("nan"))
            to_add -= 1

    return pandas.DataFrame(waiting_times)


# ----------------------- Start processes and run ---------------------------------------------------------------------
def simulate(nrobots, t, interarrival_type, arrival_rate, seed=None):
    """Simulate system with backordering and n robots.

    @:param nrobots: number of robots in the system.
    @:param t: Simulated time.
    @:interarrival_type: "exp" for exponentail (Poisson arrival) or "det" for deterministic.
    @:arrival_rate: arrival rate.
    @:param seed: random seed used to initiate pseudo random number generator.

    @:return dictionary {"W"}
     * "W" all waiting times for every order and average average
     * "AvgW" Mean value of all waiting times. (We do not use it for now).
    """
    env = simpy.Environment()
    robots = simpy.Resource(env, capacity=nrobots)
    pickers = {1: simpy.Resource(env, capacity=NUMBER_OF_PICKER), 2: simpy.Resource(env, capacity=NUMBER_OF_PICKER)}
    replenishment = simpy.Resource(env, capacity=NUMBER_OF_REPLENISHER)
    waiting_times = create_empty_waiting_times()  # Save data columnwise for performance reasons
    # Init random numbers.
    if seed is not None:
        random.seed(seed)

    # Start simulation
    env.process(source(env, robots, pickers, replenishment, waiting_times, interarrival_type, arrival_rate, total_t=t))
    env.run(until=t)
    print("Converting data to pandas")
    # Return waiting times data. and its summarized version.
    waiting_times_df = wt_to_data_frame(waiting_times)
    # waiting_times_df.to_csv("waiting-times-df.csv", index="False")  # For Debugging.
    # Calculate how often robots were actually used until the end.
    # That means number of robots
    total_busy_time = waiting_times_df.RobotBusy.sum()
    tasks_completed = waiting_times_df.RobotBusy.notnull().sum()
    # Calculate average values.
    means = waiting_times_df.mean(axis=0)
    means["TasksCompleted"] = tasks_completed  # For Debugging.
    # Calculate busy time for a single robot during the entire simulation.
    # To calculate this time from the given metrics we need to look at the world
    # from the point of view of a single robot.
    # The robots live during the entire simulation. That is "t"
    # The robots are indistinguishable, that means from all complete tasks
    # only N-th proportion of it is done by every robot. Where N is the number of robots
    # in the system: That is "total_busy_time/nrobots"
    means["SignleRobotBusyTime"] = total_busy_time / nrobots
    # From the busy time above we derive:
    means["SignleRobotIdleTime"] = t - means["SignleRobotBusyTime"]
    means["Utilization"] = means["SignleRobotBusyTime"] / t
    # Drop columns whose avearage values does not make any sense.
    means.drop(columns=["OrderId", "Arrival", "PickerId"], inplace=True)
    print("Simulation finished")
    return {"W": waiting_times_df, "Means": means}


if __name__ == "__main__":
    simulate(NUMBER_OF_ROBOTS, SIM_TIME, "exp")
# -----------------------  Data frame with all times for each order --------------------------------------------------
