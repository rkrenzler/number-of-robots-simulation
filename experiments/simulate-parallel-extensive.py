# Pod Repositioning Problem
# Copyright (C) 2019 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Create multiple simulation for different and the same number of robots.
Store resulting data in h5 files

This file performs high number of runs.

.. moduleauthor: Ruslan Krenzler

11. September 2019
"""
import datetime
from pathlib import Path
import multiprocessing

import pandas as pd

import parsim as simp
from simulation import ARRIVAL_RATE

# Number of working threads, it should be less than number of CPUS, if more than two are used.
# If number of working threads is 1, the single processed version will be called.
# We use an additional thread for managing the working threads, reading tasks and collecting results.
NWORKERS = 12
NRUNS = 200  # Number of simulation per robots.
MAX_ROBOTS = 50  # We run simulations for 1 .. MAX_ROBOTS number of robots.
# List of robots wich we plan to simulate in some point of time.
PLANNED_ROBOTS = range(17, (MAX_ROBOTS + 1))
# List of robots number which we really want to simulate now.
SIMULATE_ROBOTS_NOW = range(17, (MAX_ROBOTS + 1))

# Simulate both, exponentail (Poisson) arrivals and deterministic arrivals.
INTERARRIVAL_TYPES = ["exp", "det"]

# Different simulation times.
# Note, simulation time will be initialized when the plan is created.
# After the plan is create simulation time is ignored and the time from created plan is used.
# SIM_TIME = 3600 # An hour.
# SIM_TIME = 24*3600 # A day.
# SIM_TIME = 2*24*3600 # Two days.
# SIM_TIME = 10*24*3600 # 10 Days.
SIM_TIME = 365 * 24 * 3600  # A year.

BIG_DATA_DIR = "/media/ruslan/8feeb547-3067-4d1c-be94-e7cbffdcb210/tmp/num-of-robots-big-data"

# Input file.
SEEDS_FILE = "../data/seeds.csv"  # File with random seeds.

if Path(BIG_DATA_DIR).exists():
    HDF5_FILE = Path(BIG_DATA_DIR).joinpath("results-extensive.hdf5")
    PLAN_FILE = Path(BIG_DATA_DIR).joinpath("plan-extensive.csv")
else:
    HDF5_FILE = "../data/results-extensive.hdf5"
    PLAN_FILE = "../data/plan-extensive.csv"


def print_current_time():
    print("current time is: {}".format(datetime.datetime.now()))


def get_tasks(todo):
    """Convert the todo-Dataframe into a list of tasks.
    Where every every row of the frame is an element  of the list.
    """
    tasks = []
    nrows = todo.shape[0]
    if nrows == 0:
        return []

    for i in range(0, nrows):
        tasks.append(todo.iloc[i])

    return tasks


def worker(task):
    computation_begin = datetime.datetime.now()
    plan.loc[task.name, "ComputationBegin"] = pd.to_datetime(datetime.datetime.now())
    results = simp.do_work(task.name, task.Robots, task.Time, task.InterarrivalType,
                           task.Seed)  # Serial version for testing
    return (computation_begin, results)


if __name__ == '__main__':
    print_current_time()
    plan = simp.get_plan(SEEDS_FILE, HDF5_FILE, NRUNS, PLANNED_ROBOTS, INTERARRIVAL_TYPES, ARRIVAL_RATE, SIM_TIME)
    # Store plan for subsequent debugging.
    plan.to_csv(PLAN_FILE)
    plan = simp.make_plan_consistant(plan, HDF5_FILE)
    todo = simp.get_todo(plan, SIMULATE_ROBOTS_NOW)
    tasks = get_tasks(todo)
    # To make debugging more easy we do not use multiprocessing if NWORKERS is 1.
    if NWORKERS > 1:  # Multiprocessing version.
        pool = multiprocessing.Pool(processes=NWORKERS)
        m = multiprocessing.Manager()
        done_queue = m.Queue()
        for (cb, results) in pool.imap_unordered(worker, tasks, chunksize=1):
            plan = simp.add_simulation_results(HDF5_FILE, plan, results, computation_begin=cb)
            pool.close()
    else:  # Single process version, which is easier to debug.
        for task in tasks:
            (cb, results) = worker(task)
            plan = simp.add_simulation_results(HDF5_FILE, plan, results, computation_begin=cb)
