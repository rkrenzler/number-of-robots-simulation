# Pod Repositioning Problem
# Copyright (C) 2019 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Process simulation data stored in means.py and safe it for better processing
for publication
.. moduleauthor: Ruslan Krenzler

17. September 2019
"""
import pandas as pd

# Input file.
# Output file.
CSV_MEANS_PATH = "../data/means.csv"
CSV_MEANS_EXTENSIVE_PATH = "../data/means-extensive-18.csv"  # TODO: change later to "../data/means-extensive.csv"
CSV_MEANS_FOR_UTILIZATION_PATH = "../data/means-for-utilization.csv"
CSV_SUMMARY_PATH = "../data/summary.csv"
CSV_SUMMARY_EXP_PATH = "../data/summary-exp.csv"
CSV_SUMMARY_DET_PATH = "../data/summary-det.csv"
CSV_SUMMARY_FROM_19_EXP_PATH = "../data/summary-from-19-exp.csv"
CSV_SUMMARY_FROM_19_DET_PATH = "../data/summary-from-19-det.csv"

CSV_SUMMARY_FOR_UTILIZATION_PATH = "../data/summary-for-utilization.csv"
CSV_SUMMARY_FOR_UTILIZATION_PATH_FILENAME_ONLY = "../data/summary-for-utilization"


CSV_EXT_MEANS_SUBSET_PREFIX = "../data/external-{name}-{interarrival}.csv"
CSV_WAITING_FOR_PICKER_EXP = "../data/waiting-for-picker-exp.csv"
CSV_WAITING_FOR_PICKER_DET = "../data/waiting-for-picker-det.csv"
CSV_WAITING_FOR_REPLENISHER_EXP = "../data/waiting-for-replenisher-exp.csv"
CSV_WAITING_FOR_REPLENISHER_DET = "../data/waiting-for-replenisher-det.csv"


def summarize_for_fixed_arrival_rate():
    input = pd.read_csv(CSV_MEANS_PATH)
    # Select only robot numbers, external waiting times and turn over times.
    input = input[["Robots", "InterarrivalType", "External", "TurnOver", "Utilization"]]
    # Change  (Robots) to integer.
    input["Robots"] = input["Robots"].astype('i')
    # Calculate different statistic parameters.
    means = input.groupby(["Robots", "InterarrivalType"]).mean()
    sample_sz = input.groupby(["Robots", "InterarrivalType"]).size()
    # print(sample_sz)
    stds = input.groupby(["Robots", "InterarrivalType"]).std()
    left_cis = means - 1.96 * stds.div(sample_sz.pow(1. / 2), axis=0)
    # print(left_cis)
    right_cis = means + 1.96 * stds.div(sample_sz.pow(1. / 2), axis=0)
    df = pd.DataFrame(index=means.index)
    df["ExtMean"] = means["External"]
    df["SampleSize"] = sample_sz
    df["ExtStd"] = stds["External"]
    df["ExtCI95Left"] = left_cis["External"]
    df["ExtCI95Right"] = right_cis["External"]
    df["TOMean"] = means["TurnOver"]
    df["TOStd"] = stds["TurnOver"]
    df["TOCI95Left"] = left_cis["TurnOver"]
    df["TOCI95Right"] = right_cis["TurnOver"]
    df["UtilizationMean"] = means["Utilization"]
    df["UtilizationStd"] = stds["Utilization"]
    df["UtilizationCI95Left"] = left_cis["Utilization"]
    df["UtilizationCI95Right"] = right_cis["Utilization"]
    df.sort_values(by=["Robots", "InterarrivalType"], inplace=True)
    df.to_csv(CSV_SUMMARY_PATH)
    df = df.reset_index()
    df[df.InterarrivalType == "exp"].to_csv(CSV_SUMMARY_EXP_PATH, index=False)
    df[df.InterarrivalType == "det"].to_csv(CSV_SUMMARY_DET_PATH, index=False)


def get_utilization_csv_path(utilization: float):
    return CSV_SUMMARY_FOR_UTILIZATION_PATH_FILENAME_ONLY + f"-u{round(utilization*100)}.csv"


def summarize_for_fixed_utilization():
    input = pd.read_csv(CSV_MEANS_FOR_UTILIZATION_PATH)
    # Select only robot numbers, external waiting times and turn over times.
    input = input[["Robots", "InterarrivalType", "ArrivalRate", "AppxUtilization", "External", "TurnOver", "Utilization"]]
    # Change  (Robots) to integer.
    input["Robots"] = input["Robots"].astype('i')
    # Calculate different statistic parameters.
    means = input.groupby(["Robots", "InterarrivalType", "ArrivalRate", "AppxUtilization"]).mean()
    sample_sz = input.groupby(["Robots", "InterarrivalType", "ArrivalRate", "AppxUtilization"]).size()
    # print(sample_sz)
    stds = input.groupby(["Robots", "InterarrivalType", "ArrivalRate", "AppxUtilization"]).std()
    left_cis = means - 1.96 * stds.div(sample_sz.pow(1. / 2), axis=0)
    # print(left_cis)
    right_cis = means + 1.96 * stds.div(sample_sz.pow(1. / 2), axis=0)
    df = pd.DataFrame(index=means.index)
    df["ExtMean"] = means["External"]
    df["SampleSize"] = sample_sz
    df["ExtStd"] = stds["External"]
    df["ExtCI95Left"] = left_cis["External"]
    df["ExtCI95Right"] = right_cis["External"]
    df["TOMean"] = means["TurnOver"]
    df["TOStd"] = stds["TurnOver"]
    df["TOCI95Left"] = left_cis["TurnOver"]
    df["TOCI95Right"] = right_cis["TurnOver"]
    df["UtilizationMean"] = means["Utilization"]
    df["UtilizationStd"] = stds["Utilization"]
    df["UtilizationCI95Left"] = left_cis["Utilization"]
    df["UtilizationCI95Right"] = right_cis["Utilization"]
    df.sort_values(by=["Robots", "InterarrivalType"], inplace=True)
    df.to_csv(CSV_SUMMARY_FOR_UTILIZATION_PATH)
    df = df.reset_index()
    for ut in df.AppxUtilization.unique():
        df[df.AppxUtilization == ut].to_csv(get_utilization_csv_path(ut))

def crop_summary():
    """ Select only data with robots beginning with 19.
    The system with less 19 robots have to high variance and mean. It is hard to display."""
    df = pd.read_csv(CSV_SUMMARY_PATH)
    selection = df[(df.Robots >= 19) & (df.InterarrivalType == "exp")]
    selection.to_csv(CSV_SUMMARY_FROM_19_EXP_PATH, index=False)
    selection = df[(df.Robots >= 19) & (df.InterarrivalType == "det")]
    selection.to_csv(CSV_SUMMARY_FROM_19_DET_PATH, index=False)


def ext_subset(path, nrobots, interarrival):
    """ Extract external waitings times"""
    df = pd.read_csv(path)
    df = df[df.InterarrivalType == interarrival]
    # Convert external time from "per second" to "per hour".
    df = df[df.Robots == nrobots]["External"] / 3600
    df.name = "ExternalPerHour"
    df = pd.DataFrame(df)  # Otherwise df will loose the header "External".
    # add External data in hours
    path = CSV_EXT_MEANS_SUBSET_PREFIX.format(name=str(nrobots), interarrival=interarrival)
    df.to_csv(path, index=False)
    return df


def waiting_for_server(column_name, interarrival, dest_path):
    """Calculate statistic for particular column and store it to a csv file"""
    input = pd.read_csv(CSV_MEANS_PATH)
    # Select only corresponding interarrival types.
    input = input[input.InterarrivalType == interarrival]
    # Select only robot numbers and column name.
    input = input[["Robots", column_name]]
    # Change  (Robots) to integer.
    input["Robots"] = input["Robots"].astype('i')
    means = input.groupby(["Robots"]).mean()
    stds = input.groupby(["Robots"]).std()
    sample_sz = input.groupby(["Robots"]).size()
    left_cis = means - 1.96 * stds.div(sample_sz.pow(1. / 2), axis=0)
    right_cis = means + 1.96 * stds.div(sample_sz.pow(1. / 2), axis=0)
    df = pd.DataFrame(index=means.index)
    df["Mean"] = means[column_name]
    df["SampleSize"] = sample_sz
    df["Std"] = stds[column_name]
    df["CI95Left"] = left_cis[column_name]
    df["CI95Right"] = right_cis[column_name]
    # Subtract service and transport times from means.
    df.to_csv(dest_path)


summarize_for_fixed_arrival_rate()
crop_summary()
df = ext_subset(CSV_MEANS_EXTENSIVE_PATH, 18, "exp")
print("Mean: {0}".format(df.mean()))
print("Std: {0}".format(df.std()))
print("Sample size: {0}".format(df.size))

waiting_for_server("WaitingForPicker", "exp", CSV_WAITING_FOR_PICKER_EXP)
waiting_for_server("WaitingForPicker", "det", CSV_WAITING_FOR_PICKER_DET)
waiting_for_server("WaitingForReplenisher", "exp", CSV_WAITING_FOR_REPLENISHER_EXP)
waiting_for_server("WaitingForReplenisher", "det", CSV_WAITING_FOR_REPLENISHER_DET)

summarize_for_fixed_utilization()
