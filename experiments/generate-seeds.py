# Queueing model of a robotic mobile fulfillment system.
# Copyright (C) 2019 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Generate seeds for random generator for reproducible simulations.


We generate the seeds in advance in order to start multiple simulation simultaneously with different seeds.
Store data in a csv file with seed id and the 64 seed as an usnigned integer between 0 and 2**64.
0 is included 2**64 is excluded.

.. moduleauthor: Ruslan Krenzler.

11. September 2019
"""

import secrets # Use strong cryptographic version to generate good seeds.
import csv

NSEEDS = 32*1024 # Number of seeds.
FILENAME = "../data/seeds.csv"

with open(FILENAME, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(["Id", "Seed"])
    for id in range(1, NSEEDS + 1):
        seed = secrets.randbelow(2 ** 64)
        writer.writerow([id, seed])


