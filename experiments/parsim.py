# Pod Repositioning Problem
# Copyright (C) 2019 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Create multiple simulation for different and the same number of robots.
Store resulting data in h5 files

.. moduleauthor: Ruslan Krenzler

11. September 2019
"""
import datetime
import re
import pandas as pd
import simulation as sim


# Generate an empty simulation plan. It is a table where each rows contains
# a simulation id, seed, type of interarrivals and number of robots.
def gen_sim_plan(seed_path, nruns, robots, interarrival_types, arrival_rate, time):
    seed_db = pd.read_csv(seed_path, index_col="Id")
    plan_db = pd.DataFrame(columns=["InterarrivalType", "ArrivalRate", "Robots", "Time", "Seed",
                                    "ComputationBegin", "ComputationEnd"])
    # We will use the same simulation id as a seed id. It is a number 1,2,...
    sim_id = 1
    for interarrival in interarrival_types:
        for nrobots in robots:
            for i in range(nruns):
                seed = seed_db.loc[sim_id]["Seed"]
                # add entry to the plan.
                row = {"InterarrivalType":interarrival,
                        "ArrivalRate":arrival_rate,
                        "Robots":nrobots, "Time":time, "Seed":seed,
                        "ComputationBegin":float("NaN"), "ComputationEnd":float("NaN")}
                row = pd.DataFrame(data=row, index=[sim_id])
                plan_db = plan_db.append(row)
                sim_id += 1
    # Call raw name seem id.
    plan_db.index.name = "SimId"
    return plan_db


# Generate an empty simulation plan for a parameter rows. It is a table where each rows contains
# a simulation id, seed, parameters, ComputationBegin, ComputationEd,
def gen_sim_plan_from_parameters(seed_path, parameters):
    seed_db = pd.read_csv(seed_path, index_col="Id")
    columns = ["Seed", "ComputationBegin", "ComputationEnd"] + parameters.columns.tolist()
    plan_db = pd.DataFrame(columns=columns)
    # We will use the same simulation id as a seed id. It is a number 1,2,...
    sim_id = 1
    for parameter_row in parameters.to_dict(orient='records'):
        seed = seed_db.loc[sim_id]["Seed"]
        # add entry to the plan.
        row = parameter_row
        row["Seed"] = seed
        row["ComputationBegin"] = float("NaN"),
        row["ComputationEnd"] = float("NaN")
        row = pd.DataFrame(data=row, index=[sim_id])
        plan_db = plan_db.append(row)
        sim_id += 1
    # Call raw name seem id.
    plan_db.index.name = "SimId"
    return plan_db


def has_simulation_plan(hdf5_path):
    try:
        store = pd.HDFStore(hdf5_path)
        store.get("plan")
        store.close()
        return True
    except KeyError:
        return False


def get_existing_plan(hdf5_path):
    store = pd.HDFStore(hdf5_path)
    plan = store.get("plan")
    store.close()
    return plan


def get_plan(seed_path, hdf5_path, nruns, robots, interarrival_types, arrival_rate, time):
    """Get simulation plan from hdf5 file.

    If the simulation plan does not exists, create a new one.
    The simulation plan is a table in panda DataFrame format with columns
    "Robots", "InterarrialType", "Time", "Seed", "ComputationBegin", "ComputationEnd"
    and rows "Id". The "Id"s and "Seed"s are from seeds.csv.

    @:return simulation plan.
    """
    # Check if f contains a simulation plan.
    try:
        print("Try to open the old simulation plan")
        store = pd.HDFStore(hdf5_path)
        plan = store.get("plan")
        store.close()
    except KeyError:
        # Generate plan
        print("Generating simulation plan")
        plan = gen_sim_plan(seed_path=seed_path, nruns=nruns, robots=robots,
                            interarrival_types=interarrival_types, arrival_rate=arrival_rate,
                            time=time)
        plan.to_hdf(hdf5_path, "plan", mode="a")
    return plan


def set_plan(hdf5_path, plan):
    plan.to_hdf(hdf5_path, "plan", mode="a")


def get_ids(keys):
    # Look for all string of type simulation/sim1, simulation/sim2, ....
    # Return table ids 1, 2, ...
    ids = []
    for key in keys:
        if re.fullmatch("\/simulation\/sim[\d]+[^\d]*", key) is not None:
            numbers = re.findall("[\d]+", key)
            if len(numbers) >= 1:
                ids.append(int(numbers[0]))
    # return unque values
    return list(set(ids))


def get_ids_of_stored_simulations(path):
    """Get simulation ids of stored simulation datatas."""
    store = pd.HDFStore(path)
    ids = get_ids(store.keys())
    store.close()
    return ids


def add_simulation_results(hdf5_path, plan, results, computation_begin=None):
    # store data to simulation
    sim_id = results["SimId"]
    path = "simulation/sim{0}".format(sim_id)
    #results["W"].to_hdf(hdf5_path, path, mode="a") Do not store all results, they use too much space.
    path += "/means"
    results["Means"].to_hdf(hdf5_path, path, mode="a")

    if computation_begin is not None:
        plan.loc[sim_id, "ComputationBegin"] = computation_begin
    plan.loc[sim_id, "ComputationEnd"] = pd.to_datetime(results["ComputationEnd"])
    # Print compuation time.
    t = plan.loc[sim_id].ComputationEnd - plan.loc[sim_id].ComputationBegin
    print("Simulation time was {0}".format(t))
    # Store the plan
    plan.to_hdf(hdf5_path, "plan", mode="a")
    return plan


def do_work(sim_id, nrobots, time, iterarrival_type, arrival_rate, seed):
    # Store simulation start.
    res = sim.simulate(nrobots, time, iterarrival_type, arrival_rate, seed)
    # Add simulation_id, simulation start and simulation end to the result.
    res["SimId"] = sim_id
    res["ComputationEnd"] = pd.to_datetime(datetime.datetime.now())
    return res


def next_task(todo):
    if len(todo) > 0:
        return todo.iloc[0]
    print("No new tasks")
    return None


def make_plan_consistant(plan, path):
    """Use this function in the very beginning."""
    # Check if simulation which are marked as end actually ended.
    print("Check for missing simulations")
    sim_ids = plan[~plan.ComputationEnd.isnull()].index
    print(sim_ids)
    stored_ids = get_ids_of_stored_simulations(path)
    found = False
    for id in sim_ids:
        if id not in stored_ids:
            print("Simulation data for SimID {0} is missing. Fixing plan entry".format(id))
            plan.loc[id, "ComputationBegin"] = float("nan")
            found = True
    if not found:
        print("No missing simulation found")
    
    # Unset all computation begins if the computation end was not reached.
    # This measn simulation started but did not write all necessary values.
    mask = (~plan.ComputationBegin.isnull()) & plan.ComputationEnd.isnull()
    if sum(mask) > 0:
        print("Inconsistanet plan entries with started but not finishing simulation found")
        print(plan[mask])
        print("Correcting entries, set to NaN")
        plan.loc[mask, "ComputationBegin"] = float("nan")

    return plan


def get_todo(plan, robots):
    todo = plan[plan.ComputationBegin.isnull()]
    # Select only number of robots which are in ROBOTS
    todo = todo[todo["Robots"].isin(robots)]
    return todo

