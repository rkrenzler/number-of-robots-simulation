# Pod Repositioning Problem
# Copyright (C) 2019 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Create multiple simulation for different and the same number of robots.
Store resulting data in h5 files

.. moduleauthor: Ruslan Krenzler

31. Juli 2021
"""
import datetime
import pandas as pd
import multiprocessing

from pathlib import Path

import parsim as simp

# Run evaluate-test-system-with-fixed-utilization.R to get
# appx-arrival-rates-for-utilization.csv

# Number of working threads, it should be less than number of CPUS, if more than two are used.
# If number of working threads is 1, the single processed version will be called.
# We use an additional thread for managing the working threads, reading tasks and collecting results.
NWORKERS = 12
NRUNS = 20  # Number of simulation per robots. TODO: switch back to 20.
MIN_ROBOTS = 1
MAX_ROBOTS = 50  # We run simulations for 1 .. MAX_ROBOTS number of robots.
# List of robots wich we plan to simulate in some point of time.
PLANNED_ROBOTS = range(MIN_ROBOTS, (MAX_ROBOTS + 1))
# List of robot number nubmers and utilization which we really want to simulate now.
SIMULATE_ROBOTS_NOW = range(MIN_ROBOTS, (MAX_ROBOTS + 1))
SIMULATE_UTILIZATIONS_NOW = [0.6, 0.7, 0.8, 0.9, 0.95]

# Simulate only Poisson arrivals, that is with exponential inter-arrival times.
INTERARRIVAL_TYPES = ["exp"]

# Different simulation times.
# Note, simulation time will be initialized when the plan is created.
# After the plan is create simulation time is ignored and the time from created plan is used.
#SIM_TIME = 3600 # An hour.
#SIM_TIME = 24*3600 # A day.
# SIM_TIME = 2*24*3600 # Two days.
#SIM_TIME = 10*24*3600 # 10 Days. TODO: switch back to year.
SIM_TIME = 365 * 24 * 3600  # A year.

BIG_DATA_DIR = "/media/ruslan/8feeb547-3067-4d1c-be94-e7cbffdcb210/tmp/num-of-robots-big-data"

# Input file.
SEEDS_FILE = "../data/seeds.csv"  # File with random seeds.
ARRIVAL_RATES_CSV = "../data/appx-arrival-rates-for-utilization.csv"

if Path(BIG_DATA_DIR).exists():
    HDF5_FILE = Path(BIG_DATA_DIR).joinpath("results-for-utilization.hdf5")
    PLAN_FILE = Path(BIG_DATA_DIR).joinpath("plan-for-utilization.csv")
else:
    HDF5_FILE = "../data/results-for-utilization.hdf5"
    PLAN_FILE = "../data/plan-for-utilization.csv"


def print_current_time():
    print("current time is: {}".format(datetime.datetime.now()))


def gen_sim_parameters(arrival_rates_path, nruns, interarrival_types, time):
    """Generate simulation parameters.

    Simulate each pameter row from ARRIVAL_RATES NRUNS times
    """
    arrival_rates = pd.read_csv(arrival_rates_path)
    parameters = pd.DataFrame(columns=["InterarrivalType",  "ArrivalRate", "Time", "Robots", "AppxUtilization"])
    for interarrival_type in interarrival_types:
        for _, row in arrival_rates.iterrows():
            row["InterarrivalType"] = interarrival_type
            row["Time"] = time
            for i in range(nruns):
                parameters = parameters.append(row)
    parameters.Time = parameters.Time.astype("float64")
    parameters.Robots = parameters.Robots.astype("int32")
    return parameters


def get_tasks(todo):
    """Convert todo-Dataframe into a list of tasks.
    Where every every row of the frame is an element  of the list.
    """
    tasks = []
    nrows = todo.shape[0]
    if nrows == 0:
        return []

    for i in range(0, nrows):
        tasks.append(todo.iloc[i])

    return tasks


def worker(task):
    computation_begin = datetime.datetime.now()
    plan.loc[task.name, "ComputationBegin"] = pd.to_datetime(datetime.datetime.now())
    results = simp.do_work(task.name, task.Robots, task.Time, task.InterarrivalType, task.ArrivalRate,
                           task.Seed)  # Serial version for testing
    return (computation_begin, results)


def get_plan(seed_path, hdf5_path, arrivals_path, nruns, interarrival_types, time):
    """Get simulation plan from hdf5 file.

    If the simulation plan does not exists, create a new one.
    The simulation plan is a table in panda DataFrame format with columns
    "Robots", "InterarrialType", "Time", "Seed", "ComputationBegin", "ComputationEnd"
    and rows "Id". The "Id"s and "Seed"s are from seeds.csv.

    @:return simulation plan.
    """
    # Check if f contains a simulation plan.
    if simp.has_simulation_plan(hdf5_path):
        return simp.get_existing_plan(hdf5_path)
    else:
        print("Generating simulation plan")
        # Get arrival rates from
        parameters = gen_sim_parameters(arrivals_path, nruns, interarrival_types, time)
        plan = simp.gen_sim_plan_from_parameters(seed_path, parameters)
        # Add utilizati
        plan.to_hdf(hdf5_path, "plan", mode="a")
    return plan


def get_todo(plan, robots, utilizations):
    todo = plan[plan.ComputationBegin.isnull()]
    # Select only number of robots which are in ROBOTS
    todo = todo[todo["Robots"].isin(robots)
                & todo["AppxUtilization"].isin(utilizations)].copy()
    return todo


if __name__ == '__main__':
    print_current_time()
    plan = get_plan(SEEDS_FILE, HDF5_FILE, ARRIVAL_RATES_CSV, NRUNS, INTERARRIVAL_TYPES, SIM_TIME)
    # Store plan for subsequent debugging.
    plan.to_csv(PLAN_FILE)
    plan = simp.make_plan_consistant(plan, HDF5_FILE)
    todo = get_todo(plan, SIMULATE_ROBOTS_NOW, SIMULATE_UTILIZATIONS_NOW)
    tasks = get_tasks(todo)
    # To make debugging more easy we do not use multiprocessing if NWORKERS is 1.
    if NWORKERS > 1:  # Multiprocessing version.
        pool = multiprocessing.Pool(processes=NWORKERS)
        m = multiprocessing.Manager()
        done_queue = m.Queue()
        for (cb, results) in pool.imap_unordered(worker, tasks, chunksize=1):
            plan = simp.add_simulation_results(HDF5_FILE, plan, results, computation_begin=cb)
            pool.close()
    else:  # Single process version, which is easier to debug.
        for task in tasks:
            (cb, results) = worker(task)
            plan = simp.add_simulation_results(HDF5_FILE, plan, results, computation_begin=cb)
