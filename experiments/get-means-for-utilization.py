# Number of Robots.
# Copyright (C) 2021 Arbeitsgruppe OR an der Leuphana Universität of Lüneburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Process data generated by simulate-pralle.py and calculate means.
Store resulting data in a csv files

.. moduleauthor: Ruslan Krenzler

31. Juli 2021
"""
import os
from pathlib import Path

import statistics

BIG_DATA_DIR = "/media/ruslan/8feeb547-3067-4d1c-be94-e7cbffdcb210/tmp/num-of-robots-big-data"

# Input file.
if Path(BIG_DATA_DIR).exists():
    HDF5_RESULTS_PATH = Path(BIG_DATA_DIR).joinpath("results-for-utilization.hdf5")
else:
    HDF5_RESULTS_PATH = "../data/results-for-utilization.hdf5"


# Output file.
CSV_MEANS_PATH = "../data/means-for-utilization.csv"

if __name__ == '__main__':
    df = statistics.calculate_means(HDF5_RESULTS_PATH)
    df.to_csv(CSV_MEANS_PATH)
